using UnityEngine;

namespace Deaven.RuntimeSets
{
    public abstract class RuntimeSetSubscriber<T> : MonoBehaviour
    {
        public RuntimeSet<T> runtimeSet;
        private T _component;

        private void Awake() =>  _component = GetComponent<T>();

        private void OnEnable() => runtimeSet.Add(_component);

        private void OnDisable() => runtimeSet.Remove(_component);
    }
}