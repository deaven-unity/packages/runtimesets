using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.RuntimeSets
{
    public abstract class RuntimeSet<T> : ScriptableObject, ISerializationCallbackReceiver, IEnumerable<T>
    {
        [SerializeField] [Tooltip("Resets RuntimeValue to DefaultValue on OnAfterDeserialize")]
        private bool resetOnLoad = true;

        [SerializeField] private List<T> defaultValue = new List<T>();

        [NonSerialized, ShowInInspector, ReadOnly]
        private List<T> _runtimeValues = new List<T>();

        public int Count => _runtimeValues.Count;

        public event Action<T> ValueChanged;

        public List<T> RuntimeValues => _runtimeValues;

        public virtual void Add(T t)
        {
            if (RuntimeValues.Contains(t))
                return;

            RuntimeValues.Add(t);
            ValueChanged?.Invoke(t);
        }

        public virtual void Remove(T t)
        {
            if (!RuntimeValues.Contains(t))
                return;

            RuntimeValues.Remove(t);
            ValueChanged?.Invoke(t);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            _runtimeValues = defaultValue;
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (T t in _runtimeValues)
                yield return t;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}